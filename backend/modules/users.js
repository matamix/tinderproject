var bcrypt = require('bcryptjs');
module.exports = {
  init: function (app, tools, losDB) {
    app.get('/users/subscribe', function (req, res) {
      var saltRounds = 10; // cost factor
      var hashPass = bcrypt.hashSync(req.query.password, saltRounds); //Hash creation with bcrypt
      var email = req.query.email;
      var name = req.query.name;
      if (hashPass && email && name) {
        losDB
          .collection('Users')
          .findOne({ email: email }, function (err, document) {
            if (err) {
              tools.sendError(res, 'Error during reaching MongoDB : ' + err);
            } else if (document) {
              tools.sendError(res, 'User already exists');
            } else {

              losDB.collection('Users').insertOne(
                {
                  email: email,
                  name: name,
                  password: hashPass
                },
                function (result) {
                  if (err == null) {
                    tools.sendData(res, { id: result.insertedId }, req, losDB);
                  } else {
                    tools.sendError(
                      res,
                      'Error during inserting a user : ' + err
                    );
                  }
                }
              );
            }
          });
      } else {
        tools.sendError(res, 'Error : you need to specify all the parameters');
      }
    });

    app.get('/users/connect', function (req, res) {
      var sess = req.session;
      var email = req.query.email;
      var hash = req.query.password;

      if (sess && sess.connectedUser) {
        tools.sendError(res, 'Already connected');
        return;
      }
      losDB
        .collection('Users')
        .findOne({ email: email }, function (err, document) {
          console.log('inter')
          if (err) {
            tools.sendError(res, 'Error during reaching MongoDB : ' + err);
          } else if (document) {
            bcrypt.compare(hash, document.password).then((isSameHash) => {
              if (isSameHash) {
                sess.connectedUser = document;
                tools.sendData(
                  res,
                  {
                    id: document._id,
                    token: sess.id,
                    email: document.email,
                    name: document.name
                  },
                  req,
                  losDB
                );
              } else {
                tools.sendError(res, 'Email or password incorrect');
              }
            });
          } else {
            tools.sendError(res, 'Email or password incorrect');
          }
        });
    });


  }
};
